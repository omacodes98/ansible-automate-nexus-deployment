# Ansible - Automate Nexus Deployment 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Created a DigitalOcean server using Terraform 

* Wrote an Ansible Playbook that creates Linux user for Nexus, configures server, installs and deploys Nexus and verifies tht it is running successfully 

## Technologies Used 

* Ansible 

* Terraform 

* Nexus 

* DigitalOcean 

* Java 

* Linux 

## Steps 

SET UP 

Step 1: Create droplet with terraform 

[Terraform file](/images/01_create_droplet_with_terraform.png)

Step 2: Add droplet ip address, private key location and user you want to use to host file 

[hosts file](/images/02_add_ip_address_to_hosts_file_andprivate_key_location_and_user_you_want_access_to.png)

Step 3: Create a new playbook 

    touch deploy-node.yaml 

[Create play-book](/images/03_create_a_new_playbook_deploy_nexus.png)

INSTALL NODE AND NPM PLAY (1st)

Step 1: Name play 

Step 2: Insert host name or ip address of server you want this to be done on 

[Play 1 name and host](/images/04_name_play_and_insert_host_name_or_ip_address_of_server_you_want_this_to_be_done_on.png)

Step 3: Name first task 

Step 4: Call module for task in this case command for updating server 

[1st task](/images/05_name_first_task_and_call_module_for_task_in_this_case_command_for_updating_server.png)

Step 5: Name second task (installing java 8)

Step 6: Call apt module to install java 8

[2nd task](/images/06_name_next_task_and_call_apt_module_to_install_java_8.png)

Step 7:  Name third task (installing net tools)

Step 8: Call apt module to install net-tools 

[3rd task](/images/07_next_task_is_to_install_net_tools_firstly_name_the_task_and_call_apt_module_to_install_net-tools.png)

DOWNLOAD AND UNPACK NEXUS INSTALLER 

Step 1: Name play 

Step 2: Insert host name or ip address of server you want this to be done on 

[Play 2 name and host](/images/08_create_new_playfor_installing_nexus_name_play_and_give_it_appropriate_host.png)

Step 3: Name first task (Download nexus)

Step 4: Call module for task in this case get_url to download nexus on the server 

[DL 1st task](/images/09_create_first_task_for_downloading_and_unpacking_nexus_with_the_help_of_get_url_module.png)

Step 5: Test playbook to see if it works 

[Testing pb 1](/images/10_test_playbook.png)

[File on server](/images/11_file_on_server.png)

Step 6: Name second task (Untar nexus installer)

Step 7: Call module unarchive to untar the file 

[Dl 2nd task](/images/12_start_another_task_for_unpacking_the_file_use_unarchive_module.png)

Note: The problem with this is that nexus version is hard coded, we need to make sure everything is automated 

[Problem](/images/13_the_problem_with_this_is_that_the_nexus_version_is_hard_coded_and_that_is_not_what_we_want_we_want_everything_to_be_automated.png)

Step 8: Delete everything that was downloaded on server

Step 9: Save the output of get_url in a variable using register 

Step 10: Use debug to see the output this enables us to see the version of nexus which will be useful for setting version dynamically

[Register & debug](/images/14_delete_everything_that_was_downloaded_on_server_and_save_the_output_of_get_url_in_a_variable_using_register_and_use_debug_to_see_output_this_enable_us_to_see_the_version_of_nexus_useful_for_setting_it_dynamically.png)

Step 11: in the unarchive module change hardcoded source and reference the output key from register that displays version downloaded 

[Dynamic version](/images/15_in_the_unarchive_module_change_hardcoded_source_and_reference_the_output_key_from_register_that_displays_version_downloaded.png)

Step 12: Test playbook 

[Testing pb 2](/images/16_test_playbook.png)

[Untared folder](/images/17_untared_folders.png)

Step 13:  Name third task (find nexus folder)

[Dl 3rd task](/images/18_create_new_task_for_finding_nexus_file_to_rename_with_the_use_of_find_module.png)

Step 14: Name fourth task (Rename nexus folder)

Step 15: Use shell module to rename nexus folder which will be referenced through register of third task above  

[Renaming nexus](/images/19_create_a_new_task_to_rename_nexus_directory_using_shell_module.png)

Step 16: Name fifth task (Check if nexus folder already exist)

Step 17: Call stat module which will display info about file that could enable users to get information about file existing 

Step 18: Save the output into a variable using register 

[Dl 4th task](/images/20_create_a_new_task_that_checks_if_nxus_folder_exists_using_stat_module_and_save_it_into_a_variable_with_register.png)

Step 19: Add conditional logic to execute renaming if nexus folder doesnt exist using when and referening attribute in stat called exist

[Conditional logic](/images/21_add_conditional_logic_to_execute_renaming_if_nexus_folder_doesnt_exist_using_when_and_referencing_attribute_in_stat_called_exist.png)

[Successful playbook](/images/22_run_playbook_success.png)

CREATE NEXUS USER TO OWN NEXUS FOLDER 

Step 1: Name play 

Step 2: Insert host name or ip address of server you want this to be done on 

[Nexus user play](/images/23_create_a_play_for_creating_nexus_user_make_sure_you_insert_name_of_play_and_correct_host.png)

Step 3: Name first task  (Ensure group nexus exists)

Step 4: Call module group to create group for nexus 

[User 1st task](/images/24_create_a_task_for_previous_play_that_creates_group_for_nexus_using_group_module.png)

Step 5: Name second task (Create nexus user)

Step 6: Call user module to create nexus user 

[User 2nd task](/images/25_create_a_new_task_that_creates_nexus_user_using_the_user_module.png)

Step 7: Name third & fourth task (Make nexus user owner of nexus) (Make nexus user owner of sonatype-work)

Step 8: Call file module to change ownership of file 

[User 3rd & 4th task](/images/26_create_a_tasks_that_changes_ownership_of_nexus_and_sonatype_work_to_nexus_user_using_file_module.png)

Step 9: Test playbook to see if third play is working

[Successful play](images/27_test_play_book.png)

[File success](/images/28_folders_owners_on_server.png)

START NEXUS WITH NEXUS USER 

Step 1: Name play 

Step 2: Insert host name or ip address of server you want this to be done on 

[Start nexus play](/images/29_create_play_for_starting_nexus_with_nexus_user_make_sure_you_name_it_and_insert_host.png)

Step 3: Name first task 

Step 4: Use blockfile module to add nexus user in nexus.rc file 

[Start nexus 1st task](/images/30_add_first_task_for_play_to_add_nexus_user_in_nexusrc_file_using_blockinfile_module.png)

[Added successfully](/images/31_added_successfully.png)

Step 5: You could also use a different module like linefile for example which will just replace the line we want rather than creating another line 

[Linefile module](/images/32_we_could_also_use_a_different_module_like_linefile_for_example_which_will_just_replace_the_line_with_the_line_we_want_rather_than_creating_another_line.png)

[Test linefile](/images/33_run_playbook.png)

[Better result](/images/34_better_result.png)

Quck change 

Add conditional logic to only untar file when nexus folder doesnt exist this will prevent ansible to stop untaring the file everytime we run play if it exists 

[Untar conditional logic](/images/35_quick_change_because_we-dont_want_ansible_to_keep_untaring_the_file_when_its_done_it_once____add_conditional_logic_to_only_untar_file_when_nexus_folder_doesnt_exist.png)

Step 6: Name second task (Start nexus)

Step 7: Call command module to start nexus 

[Start nexus 2nd task](/images/36_add_the_next_task_for_the_previous_play_to_start_nexus_with_module_command.png)

Step 8: for security reasons it is better to start nexus as nexus user to be more secure so in the last play for starting nexus right underneath the host set become attribute to true and become_user to nexus so when ansible starts nexus it will start as nexus 

[Become nexus user](/images/37_for_security_reasons_we_want_to_start_nexus_as_nexus_user_to_be_more_secure_so_in_the_last_play_for_starting_nexus_right_underneath_the_host_set_become_attribute_to_true_and_become_user_to_nexus_so_when_ansible_starts_nexus_it_will_start_as_a_nexus.png)

[Test starting nexus](/images/38_run_playbook.png)

[Nexus successfully running](/images/39_nexus_successfully_running.png)

VERIFY NEXUS RUNNING

Step 1: Name play 

Step 2: Insert host name or ip address of server you want this to be done on

[Verify play](/images/40_create_a_new_play_which_will_check_if_nexus_is_running_because_we_dont_want_to_ssh_into_sserver_and_check_manually_we_want_everything_to_be_automated_make_sure_you_insert_accurate_name_and_host.png)

Step 3: Name first task (Verify nexus running)

Step 4: Call shell module to check if nexus is running using ps aux | grep nexus 

[Verify task 1](/images/41_create_task_for_play_to_check_if_nexus_is_running_with_ps_using_shell_module_then_use_theattribute_register_to_save_it_into_a_variable_and_debug_to_display_the_output.png)

Step 5: Do the same for netstat -plnt to check if nexus is running 

Step 6: Save the two outputs into variables and debug to display the output 

Step 7: Right above the netstat check task create a new task that will pause for a minute vefore executing netstat, this is because when nexus starts running it takes netstat a little bit before being able to notice it 

[Pause](/images/42_right_above_the_netstat_check_task_create_a_new_task_that_will_pause_for_a_minute_before_executing_netstat_this_is_because_when_nexus_starts_running_it_takes_netstat_a_little_bit_before_being_able_to_notice_it.png)


## Installation

    brew install ansible 

## Usage 

    ansible-playbook -i host deploy-nexus.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/ansible-automate-nexus-deployment.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-automate-nexus-deployment

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.